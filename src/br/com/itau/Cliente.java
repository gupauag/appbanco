package br.com.itau;

public class Cliente {
    private String nome;
    private int idade;
    ContaCorrente contaCorrente;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean validaIdade(int idade) {
        if (idade < 18) {
            return false;
        } else return true;
    }


}

