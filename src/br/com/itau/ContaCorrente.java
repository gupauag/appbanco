package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ContaCorrente {
    private int agencia;
    private int conta;
    private double saldo;
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }


    public void ContaCorrente() {
        this.cliente = new Cliente();
    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getConta() {
        return conta;
    }

    public void setConta(int conta) {
        this.conta = conta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public List<ContaCorrente> contasTeste() {
        List<ContaCorrente> contas = new ArrayList<ContaCorrente>();
        ContaCorrente contaCorrente;
        Cliente cliente;

        for (int i = 1; i < 11; i++) {
            contaCorrente = new ContaCorrente();
            cliente = new Cliente();
            cliente.setNome("Cliente " + i);
            cliente.setIdade(18 + i);
            contaCorrente.setCliente(cliente);
            contaCorrente.setAgencia(1);
            contaCorrente.setConta(i);
            contaCorrente.setSaldo(Math.random() * 10000);
            contas.add(contaCorrente);
        }
        return contas;
    }

    public static ContaCorrente validaConta(List<ContaCorrente> conta, int agencia, int contaCorrente) {
        ContaCorrente cont = new ContaCorrente();

        return conta.stream().
                filter(p -> p.getAgencia() == agencia && p.getConta() == contaCorrente).
                findAny().
                orElse(null);
//
//        if (cont != null) return true;
//        else return false;

    }

    private int geraContaCorrente() {
        Random random = new Random();
        int nAleatorio = random.nextInt(100);
        return nAleatorio;
    }

    public static ContaCorrente aberturaNovaConta(List<ContaCorrente> contasTeste, Cliente cliente) {
        try {
            ContaCorrente contaCorrente = new ContaCorrente();
            contaCorrente.setCliente(cliente);
            contaCorrente.setAgencia(1);
            boolean validaConta = false;
            while (!validaConta) {
                int novaconta = contaCorrente.geraContaCorrente();
                if (validaConta(contasTeste, 1, novaconta) == null) {
                    contaCorrente.setConta(novaconta);
                    validaConta = true;
                }
            }
            contaCorrente.setSaldo(0.00);
            contasTeste.add(contaCorrente);
            return contaCorrente;
        } catch (Exception e) {
            return null;
        }

    }

    public boolean setDeposito(double valor, ContaCorrente contaCorrente) {

        try {
            contaCorrente.setSaldo(contaCorrente.getSaldo() + valor);
            return true;
        } catch (Exception e) {
            return false;
        }
//        int index = contas.indexOf(contaCorrente);
//        if(index > -1){
//            contas.add(index, contaCorrente);
//            return true;
//        }else return false;
//        return true;
    }

    public boolean setSaque(double valor, ContaCorrente contaCorrente) {

        try {
            contaCorrente.setSaldo(contaCorrente.getSaldo() - valor);
            return true;
        } catch (Exception e) {
            return false;
        }
//        int index = contas.indexOf(contaCorrente);
//        if(index > -1){
//            contas.add(index, contaCorrente);
//            return true;
//        }else return false;
//        return true;
    }


}
