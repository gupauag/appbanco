package br.com.itau;

import com.sun.xml.internal.bind.v2.runtime.output.StAXExStreamWriterOutput;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Scanner;

public class IO {

    Scanner scanner = new Scanner(System.in);
    Scanner scannerChar = new Scanner(System.in);
    DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
    private List<ContaCorrente> contas;

    public IO(List<ContaCorrente> contasTeste) {
        this.contas = contasTeste;
    }

    public void menu() {
        Scanner scanner = new Scanner(System.in);
        ContaCorrente contaCorrente = new ContaCorrente();

        boolean validaMenu = false;
        int input;

        System.out.println("--- Menu Home ---");
        System.out.println("--- (1) = Abrir Conta | (2) = Login | (3) Sair do sistema");
        System.out.print("--- Digite a opção: ");
        input = scanner.nextInt();

        while (!validaMenu) {
            if (input == 1) { //abre conta
                subMenuAberturaConta();
                validaMenu = true;
            } else if (input == 2) { //Faz login
                contaCorrente = validaAcesso();
                if (contaCorrente == null) {
                    System.out.println("Você não tem conta cadastrada, tente outra vez.");
                    //direciona para criação de conta!!
                } else {
                    subMenuConta(contaCorrente);
                    validaMenu = true;
                }
            } else if (input == 3) {
                System.out.println("Sistema encerrado, obrigado e até logo!");
                validaMenu = true;
            } else {
                System.out.print("Função incorreta!");
                menu();
            }
        }
    }

    public void subMenuConta(ContaCorrente contaCorrente) {
        System.out.println("--- Menu ---");
        System.out.println("--- (1) = Consulta Saldo | (2) = Realiza Depósito | (3) = Realiza Saque | (4) = Voltar Menu");
        System.out.print("--- Digite a opção: ");
        boolean validaMenu = false;
        int input;
        input = scanner.nextInt();

        while (!validaMenu) {
            if (input == 1) { //Conulta Saldo
                imprimeExtrato(contaCorrente);
                subMenuConta(contaCorrente);
            } else if (input == 2) { //Realiza Depósito
                inputDeposito(contaCorrente);
                subMenuConta(contaCorrente);
            } else if (input == 3) { //Realiza Saque
                inputSaque(contaCorrente);
                subMenuConta(contaCorrente);
            } else if (input == 4) { // Volta p/ menu principal
                menu();
            } else {
                System.out.print("Função incorreta!");
                subMenuConta(contaCorrente);
            }
        }

    }

    public void imprimeExtrato(ContaCorrente contaCorrente) {
        decimalFormat.setRoundingMode(RoundingMode.DOWN);
        System.out.println(contaCorrente.getCliente().getNome() + " Seu saldo é de R$ " + decimalFormat.format(contaCorrente.getSaldo()));
    }

    public void inputDeposito(ContaCorrente contaCorrente) {
        decimalFormat.setRoundingMode(RoundingMode.DOWN);
        System.out.print(contaCorrente.getCliente().getNome() + ", Digite a quantia que deseja depositar: ");
        if (contaCorrente.setDeposito(scanner.nextDouble(), contaCorrente)) {
            System.out.println("Deposito realizado com sucesso");
            System.out.println("Seu novo saldo é de R$" + decimalFormat.format(contaCorrente.getSaldo()));
        } else System.out.println("Houve um problema no deposito, tente novamente.");
    }

    public void inputSaque(ContaCorrente contaCorrente) {
        decimalFormat.setRoundingMode(RoundingMode.DOWN);
        System.out.print(contaCorrente.getCliente().getNome() + ", Digite a quantia que deseja sacar: ");
        double valor = scanner.nextDouble();
        if (valor > contaCorrente.getSaldo()) {
            System.out.println("Saldo insuficiente de R$" + decimalFormat.format(contaCorrente.getSaldo()) + ", para essa quantia");
        } else if (contaCorrente.setSaque(valor, contaCorrente)) {
            System.out.println("Saque realizado com sucesso");
            System.out.println("Seu novo saldo é de R$" + decimalFormat.format(contaCorrente.getSaldo()));
        } else System.out.println("Houve um problema no Saque, tente novamente.");
    }

    public void subMenuAberturaConta() {
        ContaCorrente conta = new ContaCorrente();
        Cliente cliente = new Cliente();
        int input;
        boolean validaMenu = false;

        System.out.println("--- Menu Abertura de Conta ---");
        System.out.println("--- (1) = Abrir Conta | (3) Voltar Home");
        System.out.print("--- Digite a opção: ");
        input = scanner.nextInt();

        while (!validaMenu) {
            if (input == 1) { //abre conta
                System.out.print("Qual sua idade? :");
                int idade = scanner.nextInt();
                if (!cliente.validaIdade(idade)) {
                    System.out.println("Desculpe, vc não tem permissão para abrir uma conta.");
                    System.out.println("Para abrir uma conta vc deve teve ser maior de idade.");
                    subMenuAberturaConta();
                } else {
                    criaNovaConta(idade);
                }

                validaMenu = true;
            } else if (input == 3) {
                menu();
            } else {
                System.out.print("Função incorreta! Digite novamente uma opção: ");
                subMenuAberturaConta();
            }
        }

    }

    public void criaNovaConta(int idade) {
        ContaCorrente contacorrente = new ContaCorrente();
        Cliente cliente = new Cliente();
        System.out.println("Olá! Obrigado por escolher o Itaú como seu banco.");
        System.out.print("Digite seu nome: ");
        String nome = scannerChar.nextLine();
        cliente.setNome(nome);
        cliente.setIdade(idade);

        contacorrente = contacorrente.aberturaNovaConta(this.contas, cliente);
        if (contacorrente != null) {
            System.out.println("Conta criada com sucesso");
            System.out.println(cliente.getNome() + " seguem os seus dados para acessar sua conta.");
            System.out.println("Agencia: " + contacorrente.getAgencia() + " | C/C: " + contacorrente.getConta());
            System.out.println("Saldo = " + contacorrente.getSaldo());
        } else {
            System.out.println("Houve um problema na criação da suaa conta, tente novamente.");
        }
        subMenuAberturaConta();

    }

    public ContaCorrente validaAcesso() {
        ContaCorrente contaCorrente = new ContaCorrente();

        System.out.print("Digite sua agencia: ");
        int agencia = scanner.nextInt();
        System.out.print("Digite sua conta: ");
        int conta = scanner.nextInt();

        return contaCorrente.validaConta(this.contas, agencia, conta);
    }

}
