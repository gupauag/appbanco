package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        ContaCorrente conta = new ContaCorrente();
        List<ContaCorrente> contasTeste = new ArrayList<ContaCorrente>();
        contasTeste = conta.contasTeste(); //incinia o sistema com algumas contas/clientes

        IO io = new IO(contasTeste);

        System.out.println("Olá, como está? Selecione a opção que deseja realizar, no meu abaixo.");
        io.menu();
    }
}
